# Prueba Selección React Native

Esta prueba fue ideada para medir el nivel de conocimientos y experiencia tanto en `javascript` o `typescript`, como en el uso de APIs.

## Puntos a Valorar

- Clean Code
- Imprementación adecuada de `let`, `const`.
- Implementación de funciones anonimas
- Implementación de interpolación de `string`.
- Implementación de concepto `destructuring`.
- Implementación de valores por defecto.
- Implementación de modulos **(opcional)**
- Implementación de `reduce`, `map`, `filter`, `find`, `findIndex`, `flat`, `flatMap`, `some`.
- Implementación de `Promise`
- Implementación de `async` y `await`.
- Implementación de estado centralizado de la aplicación (Ej: Context API, Redux, Valtio, etc.)
- Implementación de [react navigation](https://reactnavigation.org/)
- Reutilización de componentes
- UX/UI (No se exige metodologias y/o herramientas, pero si que tenga un diseño y usabilidad decente)
- Implementación `typescript` o `PropTypes` **(opcional)**

## Recomendaciones

- Utilizar 2 espacios como tabulación.
- Archivos en `utf8` sin `BOM`
- Una clase, un archivo.
- Nombre de clases en UpperCamelCase
- Nombre de metodos en camelCase
- Puedes importar cualquier librería que pueda ser de ayuda para tu solución (`date-fns`, `moment`, `lodash`, etc).
- Puedes usar cualquier manejador de paquetes como `npm`, `yarn`, etc.
- No hay restricción de IDE.

## Para comenzar

Deberas solicitar acceso al proyecto mediente el enlace `Request Access` o `Solicitar Acceso` debajo del nombre del repositorio, una vez solicitado el acceso, se te concederan **48hrs** para realizar un fork y enviar el link con la solución al desafío.

## Desafío

Deberás crear un proyecto a partir de cero en `React Native`, con el cual deberás utilizar las siguientes API`s.

[Reqres](https://reqres.in) -> Simular registro e iniciar sesión

[Rick and Morty API](https://rickandmortyapi.com/api/) -> Cargar listas y detalles

El proyecto debera tener las siguientes rutas:

| Vista            | Descripción                                                                                                                                                                                                                       |
| ---------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Welcome          | Una bienvenida simple para usuarios sin cuenta                                                                                                                                                                                    |
| Login            | Debe permitir iniciar sesión con usuario y clave (simular con reqres)                                                                                                                                                             |
| Register         | Acá mostrara un formulario simple para poder registrarse (simular con reqres)                                                                                                                                                     |
| Home             | Al entrar el Home, deben cargarse 3 listas, personajes, ubicaciones y episodios (cargar desde api Rick & Morty). Ademas, debe tener una caja de texto la cual permita filtrar las listas basandose en el nombre de los elementos. |
| Detail Character | Mostrara la imagen y el detalle de un personaje especifico. El personaje selecciona desde la lista en Home.                                                                                                                       |
| Detail Location  | Mostrara el detalle de una ubicación especifica. La ubicación selecciona desde la lista en Home.                                                                                                                                  |
| Detail Episode   | Mostrara el detalle de un episodio especifico. El episodio selecciona desde la lista en Home.                                                                                                                                     |

Si alguna imagen no existiera, se puede utilizar el servicio de [placeholder](https://placeholder.com/) para reemplazarla.

# Suerte y mucho exito!

Estaremos muy contentos con tu respuesta 💪

